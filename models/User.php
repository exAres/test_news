<?php

class User extends DefaultModel {

    public $table_name = 'user';
    public $id;
    public $password;
    public $login;
    public $name;
    public $role;

    public function save() { // збереження моделі
        if(!isset($this->error)){
            $tosave = array();
            $tosave['name'] = $this->name;
            $tosave['login'] = $this->login;
            $tosave['password'] = $this->password;
            if($this->id != NULL){
                parent::update($this->table_name, $tosave);
            }else{
                parent::insert($this->table_name, $tosave);
            }
            return true;
        }else{
            return false;
        }
    }

    public function userExists($login){ // Перевірка на незайнятість логіна
        $count = self::model()->scriptFindAll("SELECT COUNT(1) FROM `user` WHERE login = '".$login."'");
        return $count[0] == '1';
    }

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
