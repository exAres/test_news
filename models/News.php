<?php

class News extends DefaultModel {

    public $table_name = 'news';
    public $id;
    public $title;
    public $description;
    public $created;
    public $author;

    public function save() { // збереження моделі
        if(!isset($this->error)){
            $tosave = array();
            $tosave['title'] = htmlspecialchars($this->title);
            $tosave['description'] = htmlspecialchars($this->description);
            $tosave['author'] = $this->author;
            if($this->id != NULL){
                parent::update($this->table_name, $tosave);
            }else{
                parent::insert($this->table_name, $tosave);
            }
            return true;
        }else{
            return false;
        }
    }

    public static function getNews($offset = 0){
        $on_page = Config::$_MVC['news']['news_on_page'];
        $news = self::model()->scriptFindAll("SELECT n.*,u.name FROM `news` n INNER JOIN `user` u ON u.id = n.author ORDER BY n.created DESC LIMIT ".$on_page." OFFSET ".$offset);
        return $news;
    }

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
