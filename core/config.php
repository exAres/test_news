<?php 
class Config{
	public static $_MVC = array(
		'database' => array(
			'host' => 'localhost',
			'user' => 'root',
			'password' => '',
			'dbname' => 'test_news'
		),
		'news' => array(
			'news_on_page' => 5,
			'preview_symbols' => 500
		)
	);
}
?>