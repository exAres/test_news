<?php
class DBC {

    private static $_instance;
    protected $_dbconnect;

    private function __construct() {
        self::connect();
    }

    private function __clone() {
        
    }

    private function connect() {
        $_dbhost = Config::$_MVC['database']['host'];
        $_dbuser = Config::$_MVC['database']['user'];
        $_dbpass = Config::$_MVC['database']['password'];
        $_dbname = Config::$_MVC['database']['dbname'];
        $this->_dbconnect = mysqli_connect($_dbhost, $_dbuser, $_dbpass, $_dbname);
        mysqli_set_charset($this->_dbconnect, 'utf8');
        // mysql_select_db($_dbname);
    }

    public static function getInstance() {
        if (self::$_instance) {
            $_self = self::$_instance;
        } else {
            $_self = self::$_instance = new DBC();
        }
        return $_self->_dbconnect;
    }

}