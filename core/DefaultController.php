<?php

class Controller {

    private $vars = array();

    public function __set($index, $value) {
        $this->vars[$index] = $value;
    }

    function redirect($url){
        header("Location: ".$url);
        die();
    }

    function render($name,$_globals = NULL) {
        $path = __SITE_PATH . '/views/' . $name . '.php';
        $head = (isset($this->header)) ? (__SITE_PATH . '/views/' .  $this->header . '.php') : '';
        $foot = (isset($this->footer)) ? (__SITE_PATH . '/views/' .  $this->footer . '.php') : '';
        if (file_exists($path) == false) {
            Route::ErrorPage404();
            return false;
        }

        foreach ($this->vars as $key => $value) {
            $$key = $value;
        }
        $GLOBALS['_CURRENT_URL'] = $GLOBALS['_PARSED_URL'];
        
        if(isset($_globals)) extract($_globals);
        if (file_exists($head)) include ($head);
        include ($path);
        if (file_exists($foot)) include ($foot);
    }

}

?>