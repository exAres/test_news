<?php

class SiteController extends Controller
{

    public $header = '/layouts/header';
    public $footer = '/layouts/footer';

    public function actionIndex()
    {
        $offset = 0;
        if(isset($_POST['offset'])) $offset = $_POST['offset'];
        $news = News::getNews($offset);
        self::render('/site/index',array("news" => $news));
    }

    public function actionView()
    {
        if(isset($_GET['id'])){
            $model = News::model()->scriptFind("SELECT n.*,u.name FROM `news` n INNER JOIN `user` u ON u.id = n.author WHERE n.id = ".$_GET['id']);
            if(!empty($model)){
                self::render('/site/view',array("item" => $model));
            }else{
                Route::ErrorPage404();  
            }
        }else{
            Route::ErrorPage404();   
        }
    }
    

}

?>