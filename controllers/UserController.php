<?php

class UserController extends Controller
{

    public $header = '/layouts/header';
    public $footer = '/layouts/footer';

    public function actionLogin() {
        if (isset($_POST['name']) && isset($_POST['pass'])) {
            if(MVC::app()->Login($_POST['name'], $_POST['pass'])){
                $result = array(
                    'status' => 'success',
                    'action' => 'login'
                );
            }else{
                $result = array(
                    'status' => 'error',
                    'action' => 'login',
                    'errors_list' => array('pass' => 'Wrong username or password')
                );
            }
            echo json_encode($result);
            die;
        }
        self::redirect('/');
    }

    public function actionLogout() {
        MVC::app()->Logout();
        self::redirect('/');
    }

    public function actionRegistration() {
        if($_POST['Registration']){
            if(User::model()->userExists($_POST['Registration']['login'])){
                $result = array(
                    'status' => 'error',
                    'action' => 'registration',
                    'errors_list' => array('Registration[login]' => 'Username allready exists')
                );
            }elseif($_POST['Registration']['password'] != $_POST['Registration']['password2']){
                $result = array(
                    'status' => 'error',
                    'action' => 'registration',
                    'errors_list' => array('Registration[password2]' => "Passwords doesn't match")
                );
            }else{
                $model = new User();
                $model->name = $_POST['Registration']['name'];
                $model->login = $_POST['Registration']['login'];
                $model->password = $_POST['Registration']['password'];
                if($model->save()){
                    $result = array(
                        'status' => 'success',
                        'action' => 'registration'
                    );
                }
            }
        }else{
            $result = array(
                'status' => 'redirect',
                'link' => '/'
            );
        }
        echo json_encode($result);
        die;
    }

    public function actionProfile() { // сторінка профілю
        if(MVC::app()->user->role != 'guest'):
            $model = null;
            if(isset($_POST['User'])){
                if($_POST['User']['id'] == MVC::app()->user->id){ 
                    $model = User::model()->findByPk($_POST['User']['id']); // редагувати існуючого
                    $model->name = $_POST['User']['name'];
                    $model->login = $_POST['User']['login'];
                    if($_POST['User']['password'] != '' || $_POST['User']['repeat'] != ''){
                        if($_POST['User']['password'] == $_POST['User']['repeat']){
                            $model->password = $_POST['User']['password'];
                        }else{
                            $model->error['repeat'] = 'Passwords not matching';
                        }
                    }
                    $model->save();
                }else{
                    Route::NotEnoughRights();
                }
            }
            self::render('/user/profile',array('model' => $model));
        else:
            Route::NotEnoughRights();
        endif;
    }

    public function actionAdd() { // сторінка додавання/редагування новини
        if(MVC::app()->user->role != 'guest'):
            $model = null;
            if(isset($_POST['News'])){ // якщо мають бути внесені зміни
                if($_POST['News']['id'] == NULL){
                    $model = new News(); // створити новину
                }else{
                    $model = News::model()->findByPk($_POST['News']['id']); // чи редагувати існуючу
                }
                if(isset($_POST['News'])){
                    $model->title = $_POST['News']['title'];
                    $model->description = $_POST['News']['description'];
                    $model->author = MVC::app()->user->id;
                    if($model->save()){
                        self::redirect('/');
                    }
                }elseif(isset($_POST['Delete'])){
                    if($model->delete()){
                        self::redirect('/');
                    }
                }
            }else{
                if(isset($_GET['id'])){
                    $model = News::model()->findByPk($_GET['id']);
                }else{
                    $model = new News();
                }
            }
            self::render('/user/add',array('model' => $model));
        else:
            Route::NotEnoughRights();
        endif;
    }

}

?>