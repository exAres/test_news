function clickEvent(element) {
    var evt = document.createEvent("MouseEvents");
    evt.initMouseEvent("click", true, true, window,
        0, 0, 0, 0, 0, false, false, false, false, 0, null);
    var a = $(element)[0];
    a.dispatchEvent(evt);
}

function customAlert(htmlText,type,timeMS) {
  if (type == undefined) {
    type = 'success'
  }
  if (timeMS == undefined) {
    timeMS = 2000;
  }
  if (htmlText == undefined) {
    htmlText = 'Error: no text message delivered'
  }
  $('body').append('<div class="alert '+type+'"><div class="alert__content"><p class="alert__text">'+htmlText+'</p></div></div>');
  var timerId = setInterval(function() {
    $('.alert').remove();
    clearInterval(timerId);
  }, timeMS);
}

function login_user(login){
  if(login){
    $('.login_check').show();
    $('.login_uncheck').hide();
  }else{
    $('.login_check').hide();
    $('.login_uncheck').show(); 
  }
}

$(function(){
    $('form.render_js').on('submit',function(){
        $.ajax({
          type: 'POST',
          url: $(this).attr('action'),
          data: $(this).serialize(),
          success: function(data){
            $("#main").html(data);
          },
          error: function(data){
          },
        });
        return false;
    });

    $('form.send_js').on('submit',function(){
        $.ajax({
          type: 'POST',
          url: $(this).attr('action'),
          data: $(this).serialize(),
          success: function(data){
            data = JSON.parse(data);
            if(data.status == 'success'){
              if(data.action == 'login'){
                login_user(true);
                clickEvent('#header');
                customAlert('Wellcome!');
              } else if(data.action == 'registration'){
                clickEvent('.modal__close-btn');
                customAlert('Registration successfully!');
              }
            } else {
              var first_error = Object.keys(data.errors_list)[0];
              $('[name="'+first_error+'"]').next().html(data.errors_list[first_error]).parent().addClass('error').on('click',function(){
                $(this).removeClass('error');
              });
            }
          },
          error: function(data){
          },
        });
        return false;
    });

    $('.back_js').on('click',function(){
      window.history.back();
    });

    $('#load_news_js').on('click',function(){
        $('#load_news_js').addClass('m-progress');
        $.ajax({
          type: 'POST',
          url: '/site/index',
          data: {
            offset: $('#load_news_js').attr('value')
          },
          success: function(data){
            $('#news_block_js').append($(data).find('#news_block_js').html());
            var count = parseInt($(data).find('#load_news_js').attr('value'),10);
            if(count < $('#load_news_js').attr('max-load')) {
              $('#load_news_js').remove();
            } else {
              $('#load_news_js').attr('value',parseInt($('#load_news_js').attr('value'),10)+count);
              $('#load_news_js').removeClass('m-progress');
            }
          },
          error: function(data){
          },
        });
    });

});