<!DOCTYPE HTML>
<html>
	<head>
		<title>News test site</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="/css/main.css" />
		<link rel="stylesheet" href="/css/style.css" />
		<!--[if lte IE 9]><link rel="stylesheet" href="/css/ie9.css" /><![endif]-->
		<!--[if lte IE 8]><link rel="stylesheet" href="/css/ie8.css" /><![endif]-->
	</head>
	<body>
		<div id="wrapper">
			<header id="header">
				<h1><a href="/">Home</a></h1>
				<nav class="links login_check" style="<?php if(MVC::app()->user->role == 'guest') echo "display:none;";?>">
					<ul>
						<li><a href="/user/profile">Profile</a></li>
						<li><a href="/user/add">Add news</a></li>
					</ul>
				</nav>
				<nav class="main">
					<ul>
						<!-- <li class="search">
							<a class="fa-search" href="#search">Search</a>
							<form id="search" method="get" action="#">
								<input type="text" name="query" placeholder="Search" />
							</form>
						</li> -->
						<li class="menu login_uncheck" style="<?php if(MVC::app()->user->role != 'guest') echo "display:none;";?>">
							<a class="fa-bars" href="#menu">Menu</a>
						</li>

						<li class="login_check" style="<?php if(MVC::app()->user->role == 'guest') echo "display:none;";?>">
							<a href="/user/logout" style="text-indent: 0em; text-align: center;"><i class="fa fa-sign-out" aria-hidden="true"></i></a>
						</li>
					</ul>
				</nav>
			</header>
			<section id="menu">
				<section>
					<form class="send_js" action="/user/login" method="post">
						<div class="input">
							<input type="text" name="name" required="true" placeholder="Login" />
							<p class="error__text"></p>
						</div>

						<div class="input">
							<input name="pass" type="password" required="true" placeholder="Password" />
							<p class="error__text"></p>
						</div>

						<input type="submit" value="Login"  class="button big fit">
					</form>

					<p style="text-align: center;">or</p>
					<a href="#" class="button big fit" onclick="modal('show', '#modal_registration')">Registration</a>
				</section>
			</section>
			<div id="main">