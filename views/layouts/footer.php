				</div>
			</div>
			<!--  modals-->
			<div class="modal close" id="modal_registration">
			  <div class="content">
			    <button class="close modal__close-btn" onclick="modal('hide', '#modal_registration')">&#10005;</button>
					<form class="send_js" action="/user/registration" method="post">
						<div class="input">
							<input type="text" id='reg_login' name="Registration[login]" required placeholder="Login" />
							<p class="error__text"></p>
						</div>

						<div class="input">
							<input type="text" id='reg_name' name="Registration[name]" required placeholder="Name" />
							<p class="error__text"></p>
						</div>

						<div class="input">
							<input type="password" id='reg_password' name="Registration[password]" required placeholder="Password" />
							<p class="error__text"></p>
						</div>
						<div class="input">
							<input type="password" id='reg_password_repeat' name="Registration[password2]" required placeholder="Repeate" />
							<p class="error__text"></p>
						</div>

						<input type="submit" name="submit" value="Registration"  class="button big fit">
					</form>
			  </div>
			</div>

		<!-- Scripts -->
			<script src="/js/jquery.min.js"></script>
			<script src="/js/skel.min.js"></script>
			<script src="/js/util.js"></script>
			<!--[if lte IE 8]><script src="js/ie/respond.min.js"></script><![endif]-->
			<script src="/js/main.js"></script>
			<script src="/js/myJS.js"></script>

	</body>
</html>