<div id='news_block_js'>
	<?php foreach($news as $item){ ?>
	<article class="post">
		<header>
			<div class="title">
				<h2><a href="/site/view/id/<?php echo $item->id; ?>"><?php echo $item->title; ?></a></h2>
			</div>
			<div class="meta">
			<?php $dt = new DateTime($item->created); ?>
				<time class="published" datetime="2015-11-01"><?php echo $dt->format('F j, Y'); ?></time>
				<div class="author"><span class="name"><?php echo $item->name; ?></span><img src="/images/profile.png" alt="" /></div>
			</div>
		</header>
		<p><?php echo nl2br(mb_substr($item->description, 0, Config::$_MVC['news']['preview_symbols'])); echo (mb_strlen($item->description) > Config::$_MVC['news']['preview_symbols']) ? "..." : ""; ?></p>
		<footer>
			<ul class="actions">
				<li><a href="/site/view/id/<?php echo $item->id; ?>" class="button big">Continue Reading</a></li>
			</ul>
			<ul class="actions login_check" <?php if(MVC::app()->user->role == 'guest') echo "hidden";?>>
				<li style="float:right;"><a class="button big" href="/user/add/id/<?php echo $item->id; ?>">Edit</a></li>
			</ul>
		</footer>
	</article>
	<?php } ?>
</div>
<button type="button" style="margin:0 auto;display: block;font-size: 110%;" id="load_news_js" max-load="<?php echo Config::$_MVC['news']['news_on_page']; ?>" value="<?php echo count($news); ?>" class="btn btn-lg btn-primary ">Load more</button>