<input class="button back_js" type="button" value=" < Back to list" style="margin-bottom: 20px; background-color: grey;" >
<article class="post">
	<header>
		<div class="title">
			<h2><?php echo $item->title; ?></h2>
		</div>
		<div class="meta">
		<?php $dt = new DateTime($item->created); ?>
			<time class="published" datetime="2015-11-01"><?php echo $dt->format('F j, Y'); ?></time>
			<div class="author"><span class="name"><?php echo $item->name; ?></span><img src="/images/profile.png" alt="" /></div>
		</div>
	</header>
	<p><?php echo nl2br($item->description); ?></p>
	<footer>
		<ul class="actions login_check" <?php if(MVC::app()->user->role == 'guest') echo "hidden";?>>
			<li style="float:right;"><a href="/user/add/id/<?php echo $item->id; ?>" class="button big">Edit</a></li>
		</ul>
	</footer>
</article>
<input class="button back_js" type="button" value=" < Back to list" style="margin-top: 0; background-color: grey;" >