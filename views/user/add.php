<?php if($model == null){ $model = new News;}?>
<form class="<!-- render_js -->" id="user_createnews" action="/user/add" method="post">
	<input type="hidden" name="News[id]" value="<?php if(isset($model->id)) echo $model->id; ?>">
	<h2 style="text-align: center;margin: 0;">Title:</h2>
	<input type="text" placeholder="Title" name="News[title]" required value="<?php if(isset($model->title)) echo $model->title; ?>" style="margin-bottom: 40px;">
	<h2 style="text-align: center;margin: 0;">Description:</h2>
	<textarea  type="text" placeholder="Description" name="News[description]" required style="resize: none;margin-bottom: 40px; height: 300px"><?php if(isset($model->id)) echo $model->description; ?></textarea>
	<input class="button" style="" type="submit" name="Save" value="Save">
	<?php if(isset($model->id)){ ?>
	<input class="button" style="margin: 0 0 0 20px;box-shadow: inset 0 0 0 1px #ff0000;color: #ff0000 !important;" type="submit" name="Delete" value="Delete">
	<?php } ?>
</form>
